# Assets styleguide

## Colors

The primary color of the delightful badge and 'delightful' text is `#cc00cc` and the tagline 'gems of freedom' has color `#000099`. They correspond respectively to 'Magenta 3' and 'Blue 5' on the standard LibreOffice palette.

All other colors are tints and shades of the primary color, created by [Color Tinter and Shader](https://txstate-etc.github.io/tints-and-shades/) (credits to [Texas State ETC](http://www.its.txstate.edu/departments/etc.html))

## Fonts

The fonts used are [Prompt](https://fonts.google.com/specimen/Prompt) for the 'delightful' text and [Rajdhani SemiBold](https://fonts.google.com/specimen/Rajdhani) for the 'gems of freedom' tagline.

These are Google fonts. If you know of good libre replacements, you can propose them in our issue tracker.

## Images

The Gem image in `delightful-logo.svg` was obtained and adapted from [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Twemoji_1f48e.svg).

The screenshot `delightful-according-to-duckduckgo.jpg` was taken by Ubuntu Screenshot v3.25.0 (credits to Emmanuele Bassi, Jonathan Blandford and Cosimo Cecchi).

## Design

The delightful badge was designed with [Inkscape](https://inkscape.org) `v0.92.5 (0.92.5+69)`.

The delightful logo was designed with [LibreOffice Draw](https://www.libreoffice.org/) `v6.0.7.3`.