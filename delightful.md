# What are delightful lists?

## Gems of freedom

The internet can be a wonderful place. A place that unites people, to exchange ideas. It allows people to cooperate and unleash their creativity. This brought forth great source of information and knowledge, open science and many beautiful free software projects to enrich our lives with. The internet has the potential to bring our minds together. To set us free.

But increasingly commercial interests are thwarting the vision of what the internet can bring to humanity. The gems of free knowledge and open source software are harder to find these day. Drowned out by marketing, advertising and SEO they do not appear in the top of our search results anymore.

Delightful lists are an effort to help bring change to this trend. To make freedom more discoverable again. This top-level project will allow navigation to all high-quality curated delightful lists created and maintained all over the web.

Anyone that wishes to do so can create their own list, and thus create an entrypoint to freedom. The list is a collection of resources (links) that can be:

- [**Open science**](https://en.wikipedia.org/wiki/Open_science): transparent and accessible knowledge that is shared and developed through collaborative networks.

- [**Free information**](https://en.wikipedia.org/wiki/Free_content): any kind of functional work, work of art, or other creative content that meets the definition of a free cultural work.

- [**Free software**](https://en.wikipedia.org/wiki/Free_software): computer software distributed under terms that allow users to run the software for any purpose as well as to study, change, and distribute it and any adapted versions.

Interested? Inspired? [Want to create your own delightful list?](delight-us.md)

## Inspired by awesome lists

The whole concept of delightful lists was inspired by the popular [awesome](https://github.com/sindresorhus/awesome) project on Github, that was started by [Sindre Sorhus](https://sindresorhus.com/). Read more in our [FAQ](faq.md).